#!/usr/bin/env python3
from neural import *
from PIL import Image
import os
import time
import chess
import pickle


def get_label(img_name):
    img_name = img_name.replace("-", "/")

    unders_pos = img_name.find("_")
    fen = img_name[0:(unders_pos if unders_pos > 0 else -12)]

    x = int(img_name[-7])
    y = int(img_name[-5])

    square = np.ravel_multi_index((y,x), (8,8)) 

    board = chess.Board(fen)

    piece = board.piece_at(square)

    if piece == None:
        return (0, 1, 0)
    
    if piece.color:
        return (1, 0, 0)

    return (0, 0, 1)




# generate single tile images of img_folder images
gen_imgs = False

# evaluate model once with test data and quit
only_evaluate = False

# show only current batch loss and accuracy per epoch
compact_visual = True

# load pretrained model from pickle object
load_trained_model = False


epochs = 30

test_count = 15 * 64 # 960 random test images never used in training from total 32000 images
img_count = 500 * 64
train_count = img_count - test_count

batch_count = 5 * 64
batches = train_count // batch_count

img_folder = "./chess-dataset-master/labeled_tile/" if not gen_imgs else "./chess-dataset-master/labeled_scaled/"
img_folder_out = "" if not gen_imgs else "./chess-dataset-master/labeled_tile_out"

model_save_path = f"{epochs}epochs_{time.strftime('%Y-%m-%d_%H:%M:%S', time.localtime())}.obj"
model_load_path = None if not load_trained_model else "chess_30epochs.obj"


img_size = 16
np.random.seed(8)
img_names = os.listdir(img_folder)

if gen_imgs:
    for i, img_name in enumerate(img_names):

        if i == 5:
            break

        im_d = Image.open(img_folder + img_name)

        c = 0
        for i in range(8):
            for j in range(8):
                name = f"{img_name}-{i}-{j}.png"
                label = get_label(name)

                if c % 2 != (i % 2) and label != (0,1,0):
                    im_tile = im_d.crop((i * img_size, (7-j) * img_size, (i * img_size) + img_size, ((7-j) * img_size) + img_size))
                    im_tile.save(f"{img_folder_out}/{name}")
                c += 1

    quit()






# Load images and labels
ims = np.empty((img_count, 3, img_size, img_size))
labels = np.empty((img_count, 3))

for i, img_name in enumerate(img_names):
    im_d = Image.open(img_folder + img_name)

    im = np.moveaxis(np.asarray(im_d), 2, 0)[0:3] / 255

    ims[i] = im
    labels[i] = get_label(img_name)


print(ims.shape, labels.shape)



# Split training and testing data
rand_test_i = np.random.choice(range(img_count), test_count, replace=False)
rand_train_i = np.ones(img_count, dtype=bool)
rand_train_i[rand_test_i] = False

X_train = ims[rand_train_i]
y_train = labels[rand_train_i]

X_test = ims[rand_test_i]
y_test = labels[rand_test_i]

print(X_train.shape, y_train.shape, X_test.shape, y_test.shape)




start = time.perf_counter()



# Initialize neural network model
if model_load_path == None:
    nn = Model([Conv(8, 3, 0), Relu(), Conv(16, 5, 0), Relu(), Flatten(), FC(3), Sigmoid()])
else:
    with open(model_load_path, 'rb') as f:
        nn = pickle.load(f)



if only_evaluate:
    print(nn.evaluate(X_test, y_test))
    quit()


# Train the model
for epoch in range(epochs):
    learning_rate = -0.00005 if epoch < 6 else (-0.00003 if epoch < 15 else (-0.000021 if epoch < 20 else -0.000015))

    print(f"\nEpoch: {epoch+1}/{epochs}  |  Learning rate: {learning_rate}\nLoss: ", end="\r" if compact_visual else "")
    
    avg_loss = 0

    for b in range(batches):
        slice_start = b * batch_count
        slice_end = (b + 1) * batch_count

        loss, acc, _ = nn.train(X_train[slice_start:slice_end], y_train[slice_start:slice_end], learning_rate)
        
        combined_loss = int(np.sum(loss))
        avg_loss += combined_loss

        if compact_visual:
            print(f"Batch: {b+1}/{batches}  |  Loss: {combined_loss}  |  Accuracy: {acc:.3f}            ", flush=True, end="\r")
        else:
            print(f"{combined_loss}", end=" ->", flush=True)


    test_acc, _ = nn.evaluate(X_test, y_test)
    print(("\n" if compact_visual else " ") + f"Avg loss: {avg_loss / batches:.3f}\nTest acc: {test_acc:.3f}")

    # Reshuffle training data and labels
    rng_state = np.random.get_state()
    np.random.shuffle(X_train)
    np.random.set_state(rng_state)
    np.random.shuffle(y_train)

end = time.perf_counter()

print(f"\nTime elapsed: {end-start}")


with open(model_save_path, 'wb') as f:
    pickle.dump(nn, f)


"""
# Fix original dataset (some images need to be rotated 90 degrees) 
# and scale images to (128,128)
for img_name in img_names:
    im_d = Image.open(img_folder + img_name)
    
    # check if pixel in (20,20) is darker than 100 even though it should be white square pixel
    rot = im_d.getpixel((20,20))[0] < 100

    im_d = im_d.resize((128,128), Image.NEAREST)

    # rotate by 90 degrees if (20, 20) square was black
    if rot:
        im_d = im_d.rotate(-90)

    im_d.save(img_folder_out + "/" + img_name)
"""
