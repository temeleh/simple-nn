#!/usr/bin/env python3
import numpy as np
from typing import Optional, Any

class Layer:
    """Abstract neural network layer"""

    def __init__(self):
        """Initializes layer specific variables"""

    def build(self, prev_shape: tuple[int, ...]) -> tuple[int, ...]:
        """Initializes parameters of the layer and sets the output shape"""
        
        #default layer has the same shape as the previous layer
        return prev_shape

    def forward(self, x: np.ndarray) -> np.ndarray:
        """
        Calculates the layer output

        :param x: previous layer output, input to this layer
        :return: output of this layer
        """

        return x

    def backward(self, da: np.ndarray) -> np.ndarray:
        """
        Calculates input (and parameter) gradients

        :param da: following layer input gradient
        :return: input gradient of this layer
        """

        return da
    
    def train(self, rate: float):
        """Updates the layer parameters using the calculated gradients and a specific learning rate"""



class ParameterLayer(Layer):
    """Abstraction of a layer with trainable weight and bias parameters"""

    def __init__(self, init_w: Optional[np.ndarray] = None, init_b: Optional[np.ndarray] = None):
        """
        Set initial weights and biases

        :param init_w: optional initial weights
        :param init_b: optional initial biases
        """

        super().__init__()
        self.params_initialized = False
        self.first_layer = False

        self.init_w = init_w
        self.init_b = init_b

    def build(self, prev_shape: tuple[int, ...]):
        """Initializes trainable parameters if not already initialized"""

        if not self.params_initialized:
            self.init_params(prev_shape)
            self.params_initialized = True

    def init_params(self, prev_shape: tuple[int, ...]):
        """Initializes the trainable parameters"""

    def train(self, rate: float):
        """
        Updates weigths and biases using the calculated gradients and the given learning rate

        :param rate: learning rate
        """

        # momentum update testing
        #self.vw = (mu * self.vw) + (rate * self.dw)
        #self.vb = (mu * self.vb) + (rate * self.db)

        self.w += rate * self.dw
        self.b += rate * self.db




class FC(ParameterLayer):
    """
    Implementation of a fully connected neural network layer


    Testing data:
    w = np.array([[1,2],[-3,2],[1,-1],[2,1]])
    b = np.array([2,0])
    
    x = np.array([[1, 2, 1, 4], [3, 1, 1, 4]])
    a = np.dot(x, w) + b

    da = np.array([[2, 1], [1, -3]])
    dw = np.einsum("ki,kj->ij", x, d) / len(x)
    db = np.sum(d, 0) / len(x)
    dx = np.einsum("ij,kj->ki", w, d)

    """

    def __init__(self, nodes: int, init_w: Optional[np.ndarray] = None, init_b: Optional[np.ndarray] = None):
        """
        Initializes the number of nodes in the layer and optionally initial weight and bias arrays.

        :param nodes: number of nodes in this fully connected layer
        :param init_w: optional initial weights
        :param init_b: optional initial biases
        """

        super().__init__(init_w, init_b)
        self.nodes = nodes
        
    def init_params(self, prev_shape: tuple[int, ...]):
        """Initialize weights and biases with the Xavier initialization"""

        self.w = self.init_w if np.any(self.init_w) else np.random.normal(0, np.sqrt(1 / prev_shape[1]), (prev_shape[1], self.nodes))
        self.b = self.init_b if np.any(self.init_b) else np.zeros(self.nodes)

    def build(self, prev_shape: tuple[int, ...]):
        """
        Sets the output shape (and initializes parameters if not already initialized). 
        The previous/input shape to this layer must be two dimensional (including the batch dimension).
        
        Data example (prev_size = prev_shape[1] = 4, connecting to 2 layer nodes):
            w = [[1, 2], [-3, 2], [1, -1], [2, 1]]              shape: (4, 2)   | (prev_size, nodes)
            b = [2, 0]                                          shape: (2)      | (nodes)

        :param prev_shape: shape of the previous layer connecting to this layer (batch_size, prev_size)
        :return: output shape of this layer (batch_size, nodes)
        """

        super().build(prev_shape)
        return (prev_shape[0], self.nodes)

    def forward(self, x: np.ndarray):
        """
        Calculates node outputs/activations for this layer with the batch input (variable x)
        from the previous layer

        Data example (4 connecting to 2 nodes with batch size 2. a is here the output array):
            x = [[1, 2, 1, 4], [3, 1, 1, 4]]                    shape: (2, 4)   | (batch_size, prev_size)
            a = [[6, 9], [11, 11]]                              shape: (2, 2)   | (batch_size, nodes)

        :param x: input to this layer
        :return: node activations (a)
        """

        self.x = x # save the input for backpropagation

        return np.dot(x, self.w) + self.b

    def backward(self, da: np.ndarray):
        """
        Calculates backpropagated gradient for this layer input, weights and biases
        with the gradients (variable da) from the following layer.

        Weight and bias gradients are saved to self.dw and self.db variables.

        Data example (4 connecting to 2 nodes with batch size 2. dx is the calculated input gradient):
            da = [[2, 1], [1, -3]]                              shape: (2, 2)   | (batch_size, nodes)
            dw = [[2.5, -4], [2.5, -0.5], [1.5, -1], [6, -4]]   shape: (4, 2)   | (prev_size, nodes)
            db = [1.5, -1]                                      shape: (2)      | (nodes)
            dx = [[4, -4, 1, 5], [-5, -9, 4, -1]]               shape: (2, 4)   | (batch_size, prev_size)

        :param da: output/activation gradients from the following layer
        :return: calculated input gradients of this layer (dx)
        """

        # example with loop (can be replaced with einsum):
        #self.dw = np.zeros(self.w.shape)
        #for i in range(len(self.x)):
        #    self.dw += np.outer(self.x[i], d[i])

        self.dw = np.einsum("ki,kj->ij", self.x, da)
        self.db = np.sum(da, 0)

        return np.einsum("ij,kj->ki", self.w, da) # sum along rows (per batch)




class Conv(ParameterLayer):
    """
    Implementation of a (2d) convolutional neural network layer.


    Testing data:
    w = np.array([[[[1,1,-1],[0,0,1],[-1,-1,-1]], [[-1,-1,1],[1,0,0],[0,0,0]], [[0,-1,1],[-1,0,0],[0,-1,0]]], 
                  [[[0,-1,0],[0,-1,-1],[0,-1,-1]], [[-1,0,1],[1,0,0],[-1,-1,-1]], [[0,1,-1],[0,-1,-1],[1,0,-1]]]])
    b = np.array([1, 0])

    x1 = np.array([[[[0,0,0],[1,0,0],[1,1,0]], [[0,0,0],[0,2,0],[0,1,0]], [[0,0,0],[1,1,0],[1,1,0]]], 
                   [[[0,0,0],[1,0,0],[1,1,0]], [[0,0,0],[0,2,0],[0,1,0]], [[0,0,0],[1,1,0],[1,1,0]]],
                   [[[0,0,0],[1,0,0],[1,1,0]], [[0,0,0],[0,2,0],[0,1,0]], [[0,0,0],[1,1,0],[1,1,0]]]])
    a1 = np.einsum("aijk,bijk->ab", x1, w) + b

    """

    def __init__(self, filters: int, filter_size: int, padding: int = 0, init_w: Optional[np.ndarray] = None, init_b: Optional[np.ndarray] = None):
        """
        Initializes the number of filters, filter size, padding and optionally initial weight and bias arrays.
        Default input padding keeps (the first two dimensions of) the output shape the same.

        :param filters: number of filters
        :param filter_size: width and height of the square filter
        :param padding: input padding, defaults to ((filter_size - 1) // 2)
        :param init_w: optional initial weights
        :param init_b: optional initial biases
        """

        super().__init__(init_w, init_b)
        self.filters = filters
        self.filter_size = filter_size
        self.stride = 1

        # input padding
        self.padding = padding if padding >= 0 else (filter_size - 1) // 2
        self.padding_total = self.padding * 2

        # output gradient padding
        self.da_padding = self.filter_size - 1 - self.padding
        self.da_padding_total = self.da_padding * 2

    def init_params(self, prev_shape: tuple[int, ...]):
        """Initialize weights and biases with He Normal initialization"""

        self.w: Any = self.init_w if np.any(self.init_w) else np.random.normal(0, np.sqrt(2 / self.filter_size**2), self.filter_shape)
        self.b = self.init_b if np.any(self.init_b) else np.zeros(self.filters)

    def build(self, prev_shape: tuple[int, ...]):
        """
        Initializes the weights and biases of the layer (variables w and b),
        sets the output shape and preallocates space for the zero padded input array and output gradient array.
        
        The previous/input shape must be 4 dimensional. Channel dimension is before the width and heigth channels.
        The last two dimensions must be the same, meaning input images/data must be square shaped.

        :param prev_shape: previous/input shape to this layer (batch, channels, prev_width, prev_height)
        :return: output shape of this layer (batch, filters, out_width, out_height)
        """

        # calculate output size, and output, input and filter shapes
        self.output_size: int = int((prev_shape[3] + self.padding_total - self.filter_size) / self.stride) + 1
        self.output_shape = (prev_shape[0], self.filters, self.output_size, self.output_size)
        self.filter_shape = (self.filters, prev_shape[1], self.filter_size, self.filter_size)
        self.input_shape = prev_shape

        # weight and bias initialization
        super().build(prev_shape)

        # preallocate zero padding for input array and output gradient array
        self.x = np.zeros((prev_shape[0], prev_shape[1], prev_shape[2] + self.padding_total, prev_shape[3] + self.padding_total))
        self.da = np.zeros((prev_shape[0], self.filters, self.output_size + self.da_padding_total, self.output_size + self.da_padding_total))

        # padding endpoints
        self.padding_end = self.padding + prev_shape[2]
        self.da_padding_end = self.da_padding + self.output_size

        return self.output_shape
        
    def forward(self, x: np.ndarray):
        """
        Calculates the output for this layer with the batch input (variable x)
        from the previous layer. Output for this layer is the cross-correlation with weights to the input.

        :param x: batch input
        :return: result of the cross-correlation with weights to the batch input
        """

        # save input with padding
        self.x[:,:,self.padding:self.padding_end,self.padding:self.padding_end] = x

        return self._convolve("aijk,bijk->ab", self.output_shape, self.filter_size, self.x, self.w, self.b)

    def backward(self, da: np.ndarray):
        """
        Calculates backpropagated gradient for this layer input, weights and biases
        with the gradients (variable da) from the following layer.

        Weight and bias gradients are saved to self.dw and self.db variables.

        :param da: output/activation gradients from the following layer
        :return: calculated input gradients of this layer (dx) or None if this is the first layer
        """

        # convolution of input and loss gradients -> weight filter gradients
        self.dw = self._convolve("cbij,caij->ab", self.filter_shape, self.output_size, self.x, da) 
        self.db = np.sum(da, axis=(0,2,3))

        # skip input gradient calculation if this is the first layer
        if self.first_layer:
            return None

        # padded loss gradients
        self.da[:,:,self.da_padding:self.da_padding_end, self.da_padding:self.da_padding_end] = da

        # convolution of padded loss gradients and flipped filters -> input gradients
        return self._convolve("acij,cbij->ab", self.input_shape, self.filter_size, self.da, self.w[:,:,::-1,::-1]) 

    def _convolve(self, operation: str, output_shape: tuple[int, ...], filter_size: int, inp, filt, bias = 0):
        """
        Applies a given einsum operation for each sliding window in input array 'inp' of size 'filter_size' with filter 'filt'
        and optionally adds a bias to the result. 

        :param operation: einsum operation to apply to each sliding window
        :param output_shape: output shape of the convolution
        :param filter_size: size of the convolved filter/window (width and heigth)
        :param inp: array to be convolved
        :param filt: array to apply to each sliding window of inp
        :param bias: optional bias to apply to the einsum operation result

        :return: array of convolved einsum operation results
        """

        a = np.empty(output_shape)

        for i in range(output_shape[2]):
            for j in range(output_shape[2]):
                a[:,:,i,j] = np.einsum(operation, inp[:,:,i:(i + filter_size), j:(j + filter_size)], filt) + bias

        return a



class MaxPool(Layer):
    """
    Implementation of a non-overlapping max pooling layer
    """

    def __init__(self, size: int):
        """
        Initializes the size of the pooling window (width and height).
        Input image dimensions must be divisible by this size (this pooling is always non-overlapping)

        :param size: size of the pooling window (width and height)
        """

        super().__init__()
        self.size = size
        self.flat_size = size ** 2

    def build(self, prev_shape: tuple[int, ...]):
        """
        (Re)calculate output size and save the previous layer size and 
        initialize empty array where the maximum indexes are saved.

        :param prev_shape: output shape of the previous layer
        :return: output shape of this layer
        """

        self.prev_shape = prev_shape
        self.output_size = prev_shape[3] // self.size
        self.flat_batch_channels = prev_shape[0] * prev_shape[1]

        # array to save maximum indexes into (indexes that have the maximum values)
        self.ind = np.empty((self.flat_batch_channels, self.output_size, self.output_size))

        return (prev_shape[0], prev_shape[1], self.output_size, self.output_size)

    def forward(self, x: np.ndarray):
        """
        Calculates the max pooled/scaled output for the input x.

        Reduces each pooling window slice of the input to the max value of that window and
        saves the indexes of those max values for the backward pass.

        :param x: previous layer output
        :return: output of this layer
        """

        a = np.empty((x.shape[0], x.shape[1], self.output_size, self.output_size))

        # This could be cleaner and more efficient... 
        # I couldn't find an easy way to combine this pooling into a single operation while calculating indexes for backpropagation (with numpy only)
        for i, ii in zip(range(0, x.shape[3], self.size), range(self.output_size)):
            for j, jj in zip(range(0, x.shape[3], self.size), range(self.output_size)):
                win = x[:,:, i:(i + self.size), j:(j + self.size)].reshape(self.flat_batch_channels, self.flat_size) # reshaped flat window
                self.ind[:, ii, jj] = win.argmax(axis=1) # get maximum indexex

                # get maximum values with the indexes
                a[:,:,ii,jj] = np.array([win[f].take(self.ind[f, ii, jj]) for f in range(self.flat_batch_channels)]).reshape(x.shape[0], x.shape[1])
                
        return a

    def backward(self, da: np.ndarray):
        """
        Calculates the input gradient using the indexes from the forward pass.

        The following layer gradient directly flows to the max indexes of the pooling windows
        and it is zero everywhere else.

        :param da: following layer input gradient
        :return: input gradient of this layer
        """

        # previous layer output shape, possibly with a new batch size
        d = np.zeros((da.shape[0], *self.prev_shape[1:4])) 

        for i, ii in zip(range(0, self.prev_shape[3], self.size), range(self.output_size)):
            for j, jj in zip(range(0, self.prev_shape[3], self.size), range(self.output_size)):
                win = d[:,:, i:(i + self.size), j:(j + self.size)].reshape(self.flat_batch_channels, self.flat_size) # reshaped flat window
                da_flat = da[:,:,ii,jj].reshape(self.flat_batch_channels)

                # put gradient to indexes that had maximum indexes
                for f in range(self.flat_batch_channels):
                    win[f].put(self.ind[f, ii, jj], da_flat[f])

                # reshape flat window back to non-flat indexes
                d[:,:, i:(i + self.size), j:(j + self.size)] = win.reshape(*self.prev_shape[0:2], self.size, self.size)

        return d
        


class Relu(Layer):
    """
    Implementation of a ReLU neural network layer (max(0, x))
    """

    def forward(self, x: np.ndarray):
        """
        Calculates the layer output.
        Also precalculates a part of the input gradient and saves it to dx variable.
        (partial derivatives to ReLU output, not to the loss function)

        :param x: previous layer output
        :return: output of this layer
        """

        self.dx = np.where(x > 0, 1, 0)
        return np.maximum(0, x)

    def backward(self, da: np.ndarray):
        return self.dx * da


class Sigmoid(Layer):
    def forward(self, x: np.ndarray):
        s = 1.0 / (1.0 + np.exp(-x))
        self.ds = s * (1.0 - s)
        return s

    def backward(self, da: np.ndarray):
        return self.ds * da


class Tanh(Layer):
    def forward(self, x: np.ndarray):
        t = np.tanh(x)
        self.dt = 1.0 - (t ** 2)
        return t

    def backward(self, da: np.ndarray):
        return self.dt * da



class Flatten(Layer):
    """Flatten the layer input into two dimensions (batch dimension and one output dimension)"""

    def build(self, prev_shape: tuple[int, ...]) -> tuple[int, ...]:
        self.prev_shape = prev_shape
        self.output_shape = (prev_shape[0], np.prod(prev_shape[1:]))
        
        return self.output_shape

    def forward(self, x: np.ndarray) -> np.ndarray:
        return np.reshape(x, self.output_shape)

    def backward(self, da: np.ndarray) -> np.ndarray:
        return np.reshape(da, self.prev_shape)
    



class Model:
    """
    Classifier neural network model that implements functions to do operations to 
    a list of neural network layers, like predict and train with specified input data.
    """

    def __init__(self, layers: list[Layer]):
        """
        Initializes the model with the input data shape and layers.

        :param input_shape: input shape
        :param layers: list of neural network layers
        """
        self.layers = layers
        self.input_shape = None

    def build(self, input_shape: tuple[int, ...]):
        """
        Calls build on every layer sequentially and gives the layer output shape to the next layer.

        :param input_shape: shape of the input data
        """

        prev_shape = input_shape
        for i, layer in enumerate(self.layers):
            # Signal to the first layer that it doesn't need to calculate input gradients
            if i == 0 and isinstance(layer, ParameterLayer):
                layer.is_first_layer = True
            
            prev_shape = layer.build(prev_shape)

    def __call__(self, x: np.ndarray):
        """
        Forward pass of the network that calculates the layer activations for a single batch.

        :param x: input data batch
        :return: last layer activations/outputs
        """

        # Rebuild layers if input shape is not set or it changes
        if self.input_shape == None or self.input_shape != x.shape:
            self.build(x.shape)

        # Forward pass for each layer
        a = x
        for layer in self.layers:
            a = layer.forward(a)

        return a

    def predict(self, a: np.ndarray):
        """
        Calculates the prediction for this input data batch with the last layer activations.

        :param a: last layer activations
        :return: prediction label index
        """
        
        return np.argmax(a, axis=1)


    def evaluate(self, x: np.ndarray, y: np.ndarray, a: Optional[np.ndarray] = None):
        """
        Evaluates network accuracy performance with specific input data and output labels.

        :param x: input data
        :param y: output labels
        :param a: optional precalculated activations for this input data
        :return: tuple of accuracy, (percentage of correct predictions) and the number of correct predictions
        """

        # calculate activations if not given
        if not np.any(a):
            a = self(x)

        correct_pred = 0
        for (pred, label) in zip(self.predict(a), y):
            if label[pred] == 1:
                correct_pred += 1

        return correct_pred / len(x), correct_pred

    def train(self, x: np.ndarray, y: np.ndarray, rate: float):
        """
        Trains the network with given input data, corresponding output labels and a learning rate.

        :param x: input data
        :param y: output labels
        :param rate: learning rate
        :return: loss, evaluation and a tuple of last layer activations and partial loss gradients
        """

        a = self(x)
        loss, dy = self.loss(a, y)
        
        d = dy
        for layer in reversed(self.layers):
            d = layer.backward(d)
            layer.train(rate)

        return loss, self.evaluate(x, y, a)[0], (a, dy)


    def loss(self, a: np.ndarray, y: np.ndarray) -> tuple:
        """
        Returns loss and its partial derivatives for 
        specific output layer activations and corresponding output labels

        :param a: last layer outputs/activations
        :param y: correct outputs for corresponding input data
        :return: 2-tuple of loss and its partial derivatives
        """

        return (np.sum((a - y)**2, 1), 2*(a - y))
