#!/usr/bin/env python3
from neural import *
import torch
import time

def test_basic():
    x = np.array([2.0, 1.0])

    w1 = np.array([[0.5, 1.0, 0.5], [1.0, -3.0, 1.0]])
    b1 = np.array([2.0, 0.0, 1.0])
    w2 = np.array([[1.0, 1.0], [0.5, 1.0], [1.0, 0.5]])
    b2 = np.array([0.0, 1.0])

    nn = Model([FC(3, w1, b1), Relu(), FC(2, w2, b2), Relu()])

    loss, _, (a, dy) = nn.train(np.array((x,x)), np.array([[5, 2],[5, 2]]), -0.01)
    
    assert loss[0] == 24.25
    assert np.all(dy[0] == np.array((4,9)))
    assert np.all(a[0] == np.array((7, 6.5)))


def test_fc():
    # common initialization (input, weights)
    x = np.array([[2.0, 1.0], [1.3, -3.95], [-1.0, 2.0]])

    w = np.array([[0.5, 1.0, 0.5], [1.0, -3.0, 1.0]])
    b = np.array([2.0, 0.0, 1.0])

    d = np.array([[1.0, 2.0, 1.0], [1.0, 2.0, 5.0], [2.0, 0.5, 1.0]]) # loss gradient 


    # torch setup    
    tx = torch.tensor(x)
    tx.requires_grad = True

    tlin = torch.nn.Linear(2, 3)
    tlin.weight = torch.nn.Parameter(torch.tensor(np.moveaxis(w, 0, 1)))
    tlin.bias = torch.nn.Parameter(torch.tensor(b))

    trel = torch.nn.ReLU()


    # neural.py setup
    fc = FC(3, init_w=w, init_b=b)
    fc.build(x.shape)
    
    rel = Relu()


    # torch output (forward)
    tout = trel(tlin(tx))

    # neural.py output (forward)
    out = rel.forward(fc.forward(x))


    # assert that outputs are the same
    print(tout, out, sep="\n---\n", end="\n\n###\n\n")
    assert np.array_equal(tout.detach().numpy(), out)

    
    # torch gradients (backward)
    tout.backward(torch.tensor(d))

    # neural.py gradients (backward)
    dx = fc.backward(rel.backward(d))


    # assert that gradients are the same
    print(tx.grad, dx, tlin.weight.grad, fc.dw, tlin.bias.grad, fc.db, sep="\n---\n", end="\n\n")
    assert np.array_equal(tx.grad.detach().numpy(), dx)
    assert np.array_equal(tlin.weight.grad.detach().numpy().T, fc.dw)
    assert np.array_equal(tlin.bias.grad.detach().numpy(), fc.db)


def test_conv():
    x, w, b, d, d_pad = conv_init()

    padding = 0
    batch_size = 3
    #d = d_pad
    x = x[0:batch_size]
    d = d[0:batch_size]


    # torch setup
    tx = torch.tensor(x)
    tx.requires_grad = True

    tconv = torch.nn.Conv2d(4, 3, 2, bias=True, padding=padding)
    tconv.weight = torch.nn.Parameter(torch.tensor(w))
    tconv.bias = torch.nn.Parameter(torch.tensor(b))
    
    trel = torch.nn.ReLU()

    # neural.py setup
    conv = Conv(3, 2, padding, init_w=w)
    conv.build(x.shape)

    rel = Relu()
    

    # torch output (forward)
    tout, tperf = run_perf(lambda: trel(tconv(tx)))

    # neural.py output (forward)
    out, perf = run_perf(lambda: rel.forward(conv.forward(x)))


    print(tout, out, tperf, perf, sep="\n---\n", end="\n\n###\n\n")
    assert np.array_equal(tout.detach().numpy(), out)


    # torch gradients (backward)
    _, tperf = run_perf(lambda: tout.backward(torch.tensor(d)))

    # neural.py gradients (backward)
    dx, perf = run_perf(lambda: conv.backward(rel.backward(d)))


    print(tx.grad, dx, tconv.weight.grad, conv.dw, tconv.bias.grad, conv.db, tperf, perf, sep="\n---\n", end="\n\n")
    assert np.array_equal(tx.grad.detach().numpy(), dx)
    assert np.array_equal(tconv.weight.grad.detach().numpy(), conv.dw)
    assert np.array_equal(tconv.bias.grad.detach().numpy(), conv.db)

def test_pool():
    x, d = pool_init()

    batch_size = 3
    x = x[0:batch_size]
    d = d[0:batch_size]


    # torch setup
    tx = torch.tensor(x)
    tx.requires_grad = True

    tpool = torch.nn.MaxPool2d(2, 2)
    
    # neural.py setup
    pool = MaxPool(2)
    pool.build(x.shape)
    

    # torch output (forward)
    tout, tperf = run_perf(lambda: tpool(tx))

    # neural.py output (forward)
    out, perf = run_perf(lambda: pool.forward(x))

    print(tout, out, tperf, perf, sep="\n---\n", end="\n\n###\n\n")
    assert np.array_equal(tout.detach().numpy(), out)


    # torch gradients (backward)
    _, tperf = run_perf(lambda: tout.backward(torch.tensor(d)))

    # neural.py gradients (backward)
    dx, perf = run_perf(lambda: pool.backward(d))


    print(tx.grad, dx, tperf, perf, sep="\n---\n", end="\n\n")
    assert np.array_equal(tx.grad.detach().numpy(), dx)



def run_perf(f):
    start = time.perf_counter()
    out = f()
    end = time.perf_counter()
    return out, end - start

def conv_init():
    x = np.array(
    [
        [
            [
                [0,0,0],
                [1,0,0],
                [1,1,0]
            ], 
            [
                [0,0,0],
                [0,20,0],
                [0,1,0]
            ], 
            [
                [0,0,0],
                [1,1,0],
                [1,1,0]
            ], 
            [
                [0,0,0],
                [1,1,0],
                [1,1,0]
            ]
        ], 
        [
            [
                [0,0,0],
                [1,0,0],
                [1,1,0]
            ], 
            [
                [0,0,0],
                [54,50,0],
                [0,1,0]
            ], 
            [
                [0,0,0],
                [1,1,0],
                [1,1,0]
            ], 
            [
                [0,0,0],
                [1,1,0],
                [1,1,0]
            ]
        ], 
        [
            [
                [0,0,0],
                [1,0,0],
                [1,1,0]
            ], 
            [
                [0,0,0],
                [0,50,0],
                [0,1,0]
            ], 
            [
                [0,0,0],
                [1,1,0],
                [1,1,0]
            ], 
            [
                [0,5,0],
                [1,1,0],
                [4,1,0]
            ]
        ], 
        [
            [
                [0,0,0],
                [1,0,0],
                [1,1,0]
            ], 
            [
                [0,0,4],
                [7,50,0],
                [0,1,0]
            ], 
            [
                [5,0,0],
                [1,1,0],
                [1,1,0]
            ], 
            [
                [0,0,0],
                [1,1,4],
                [1,1,0]
            ]
        ]
    ]).astype(np.float32)

    w = np.array(
    [
        [
            [
                [1,1],
                [-1,-1]
            ], 
            [
                [-1,1],
                [0,0]
            ], 
            [
                [0,-1],
                [0,0]
            ], 
            [
                [1,-1],
                [-1,1]
            ]
        ], 
        [
            [
                [0,0],
                [0,-1]
            ], 
            [
                [-1,1],
                [-1,-1]
            ], 
            [
                [0,-1],
                [1,-1]
            ], 
            [
                [1,-1],
                [-1,1]
            ]
        ], 
        [
            [
                [1,0],
                [0,-1]
            ], 
            [
                [-1,1],
                [-1,-1]
            ], 
            [
                [0,1],
                [1,-1]
            ], 
            [
                [1,-1],
                [1,1]
            ]
        ]
    ]).astype(np.float32)

    b = np.array([0,0,0]).astype(np.float32)



    d_pad = np.array([
       [[[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 2., 1.],
         [1., 1., 1., 1.]],

        [[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]],

        [[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]]],


       [[[1., 1., 1., 1.],
         [3., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]],

        [[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]],

        [[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]]],


       [[[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]],

        [[1., 1., 1., 1.],
         [1., 4., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]],

        [[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]]],


       [[[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]],

        [[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]],

        [[1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.],
         [1., 1., 1., 1.]]]
    ]).astype(np.float32)

    d = np.array([
       [[[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 5.]]],


       [[[1., 1.],
         [1., 50.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]]],


       [[[1., 1.],
         [2., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]]],


       [[[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]]]
    ])

    return x, w, b, d, d_pad


def pool_init():
    x = np.array(
    [
        [
            [
                [0,1,0,1],
                [1,0,0,0],
                [1,1,1,0],
                [1,1,3,1]
            ], 
            [
                [0,1,0,1],
                [1,1,0,0],
                [2,0,1,0],
                [1,1,0,1]
            ], 
            [
                [1,0,0,1],
                [1,0,0,0],
                [0,0,1,0],
                [1,1,0,1]
            ], 
            [
                [0,0,0,1],
                [1,0,5,0],
                [1,0,1,0],
                [1,1,0,1]
            ]
        ], 
        [
            [
                [0,1,0,1],
                [1,0,0,0],
                [1,1,1,0],
                [1,1,3,1]
            ], 
            [
                [0,1,0,1],
                [1,1,0,0],
                [2,0,2,0],
                [1,1,0,1]
            ], 
            [
                [1,0,0,1],
                [1,0,0,0],
                [2,0,1,0],
                [1,1,0,1]
            ], 
            [
                [0,0,0,1],
                [1,0,5,0],
                [2,0,1,0],
                [1,1,0,1]
            ]
        ], 
        [
            [
                [0,1,0,1],
                [1,0,0,0],
                [1,3,1,0],
                [1,1,3,1]
            ], 
            [
                [0,1,0,1],
                [1,1,0,0],
                [2,0,1,0],
                [1,1,0,1]
            ], 
            [
                [1,0,0,1],
                [1,0,0,0],
                [0,0,1,0],
                [1,1,0,1]
            ], 
            [
                [0,0,0,1],
                [1,0,5,0],
                [1,3,1,0],
                [1,1,0,1]
            ]
        ], 
        [
            [
                [0,1,0,1],
                [1,0,0,0],
                [1,1,1,0],
                [1,1,3,1]
            ], 
            [
                [0,1,0,1],
                [1,1,0,0],
                [2,0,1,0],
                [1,1,0,1]
            ], 
            [
                [1,0,0,1],
                [1,0,0,0],
                [0,2,1,0],
                [1,1,0,1]
            ], 
            [
                [0,0,0,1],
                [1,0,5,0],
                [1,0,1,0],
                [1,6,0,1]
            ]
        ]
    ]).astype(np.float32)

    

    d = np.array([
       [[[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 5.]]],


       [[[1., 1.],
         [1., 50.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]]],


       [[[1., 1.],
         [2., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]]],


       [[[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]],

        [[1., 1.],
         [1., 1.]]]
    ])

    return x, d


if __name__ == '__main__':
    test_basic()

    test_fc()
    test_conv()
    test_pool()

